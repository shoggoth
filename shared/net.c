/* Shoggoth - Distributed File System
 * Network module
 *
 * Copyright (C) 2012 Pawel Dziepak
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

#include "common.h"
#include "net.h"

#define NET_IS_ELDER		5
#define NET_MAX_ELDERS		0

#define NET_GUARD_RES		5

#define NET_MAX_DATA_SIZE	1024 * 64
#define NET_MAX_UDP_SIZE	1440

#define NET_TCP_THREAD_POOL	8
#define NET_UDP_THREAD_POOL	4

#define NET_REPLY_TIMEOUT	2

struct node_address {
	uint32_t ipv4;
	uint16_t port;

	int sock;
	uint8_t udp_tag;
	pthread_mutex_t sock_lock;
	time_t last_used;

	struct node_address *elder_prev, *elder_next;
};

struct response_address {
	int sock;
	struct sockaddr_in addr;

	uint8_t is_udp;
	uint8_t tag;
};

struct packet_header {
	uint32_t size;
} __attribute__((packed));

enum {
	UDP_REQUEST	= 1,
	UDP_REPLY	= 2
};

struct udp_packet_header {
	uint16_t size;
	uint8_t flag;
	uint8_t tag;
} __attribute__((packed));

struct udp_request_entry {
	struct sockaddr_in *addr;
	uint8_t tag;
	int valid;

	pthread_cond_t event;
	pthread_mutex_t event_lock;

	void *buffer;
	uint32_t bsize;

	struct udp_request_entry *next, *prev;
};

static struct udp_request_entry *udp_rlist = NULL;
static pthread_mutex_t udp_rlist_lock = PTHREAD_MUTEX_INITIALIZER;

static struct node_address *elder_first = NULL, *elder_last = NULL;
static pthread_mutex_t elders_lock = PTHREAD_MUTEX_INITIALIZER;

static int conn_count = 0;
static pthread_mutex_t conn_lock = PTHREAD_MUTEX_INITIALIZER;

static int udp_socket;

static pthread_t guardian;
static struct node_address *guarded = NULL;
static pthread_mutex_t guard_lock = PTHREAD_MUTEX_INITIALIZER;

static net_process_request process_request;

struct thread_pool_request {
	struct response_address *addr;

	struct thread_pool_request *next;
};
static int thpool_count = 0, thpool_busy = 0;
static struct thread_pool_request *thpool_rlist = NULL;
static pthread_mutex_t thpool_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t thpool_event = PTHREAD_COND_INITIALIZER;

struct udp_thread_pool_request {
	struct response_address *addr;
	void *buffer;

	struct udp_thread_pool_request *next;
};
static struct udp_thread_pool_request *udp_thpool_rlist = NULL;
static struct udp_thread_pool_request *udp_thpool_rlist_end = NULL;
static pthread_mutex_t udp_thpool_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t udp_thpool_event = PTHREAD_COND_INITIALIZER;

void net_elder_list_rem(struct node_address *addr) {
	if (addr->elder_prev)
		addr->elder_prev->elder_next = addr->elder_next;
	else
		elder_first = addr->elder_next;

	if (addr->elder_next)
		addr->elder_next->elder_prev = addr->elder_prev;
	else
		elder_last = addr->elder_prev;
}

void net_elder_list_add(struct node_address *addr) {
	addr->elder_next = NULL;
	if (elder_last)
		elder_last->elder_next = addr;
	if (!elder_first)
		elder_first = addr;
	addr->elder_prev = elder_last;
	elder_last = addr;
}

int net_set_buffer_size(int sock) {
	size_t buffsize = NET_MAX_DATA_SIZE;
	socklen_t len = sizeof(buffsize);

	if (setsockopt(sock, SOL_SOCKET, SO_SNDBUF, &buffsize,
			sizeof(buffsize)) < 0)
		return -errno;

	if (getsockopt(sock, SOL_SOCKET, SO_SNDBUF, &buffsize, &len) < 0)
		return -errno;

	if (buffsize < NET_MAX_DATA_SIZE)
		return -EMSGSIZE;

	if (setsockopt(sock, SOL_SOCKET, SO_RCVBUF, &buffsize,
			sizeof(buffsize)) < 0)
		return -errno;

	if (getsockopt(sock, SOL_SOCKET, SO_RCVBUF, &buffsize, &len) < 0)
		return -errno;

	if (buffsize < NET_MAX_DATA_SIZE)
		return -EMSGSIZE;

	return 0;
}

int net_connection_exist(struct node_address *addr) {
	return addr->sock != -1;
}

int net_connect(struct node_address *addr) {
	struct sockaddr_in serv_addr;
	int err, sock;

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0)
		return -errno;

	err = net_set_buffer_size(sock);
	if (err < 0)
		return err;

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = addr->ipv4;
	serv_addr.sin_port = addr->port;
	if (connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
		return -errno;

	addr->sock = sock;

	pthread_mutex_lock(&conn_lock);
	conn_count++;
	pthread_mutex_unlock(&conn_lock);

	return 0;
}

int net_disconnect(struct node_address *addr) {
	pthread_mutex_lock(&conn_lock);
	conn_count--;
	pthread_mutex_unlock(&conn_lock);

	if (close(addr->sock) < 0)
		return -errno;

	addr->sock = -1;

	return 0;
}

int net_acquire_connection(struct node_address *addr) {
	int err;

	err = pthread_mutex_lock(&addr->sock_lock);
	if (err != 0)
		return -err;

	if (addr->sock == -1)
		return net_connect(addr);

	pthread_mutex_lock(&elders_lock);
	if (addr != guarded)
		net_elder_list_rem(addr);
	pthread_mutex_unlock(&elders_lock);

	return 0;
}

void net_release_connection(struct node_address *addr) {
	addr->last_used = time(NULL);

	pthread_mutex_lock(&elders_lock);
	if (addr != guarded)
		net_elder_list_add(addr);
	pthread_mutex_unlock(&elders_lock);

	pthread_mutex_unlock(&addr->sock_lock);
}

void *net_elders_collector(void *args) {
	struct node_address *addr;
	int wait_time = NET_IS_ELDER;
	(void)args;

	while (1) {
		sleep(wait_time);

		if (conn_count < NET_MAX_ELDERS || !elder_first
						|| guarded == elder_first) {
			wait_time = min(max(wait_time * 2, NET_IS_ELDER),
						NET_IS_ELDER * 16);
			continue;
		}

		pthread_mutex_lock(&elders_lock);
		if (pthread_mutex_trylock(&elder_first->sock_lock) == EBUSY) {
			pthread_mutex_unlock(&elders_lock);
			continue;
		}

		if (elder_first->last_used > time(NULL) - NET_IS_ELDER) {
			pthread_mutex_unlock(&elders_lock);
			pthread_mutex_unlock(&elder_first->sock_lock);
			continue;
		}

		addr = elder_first;
		elder_first = addr->elder_next;
		if (addr->elder_next)
			addr->elder_next->elder_prev = NULL;
		if (elder_last == addr)
			elder_last = NULL;
		pthread_mutex_unlock(&elders_lock);

		net_disconnect(addr);
		pthread_mutex_unlock(&addr->sock_lock);

		wait_time = max(min(wait_time / 2, NET_IS_ELDER), 1);
	}
}

int net_create_node_address(struct node_address **addr, const char *ipv4,
				uint16_t port) {
	int err;
	err = inet_pton(AF_INET, ipv4, &(*addr)->ipv4);
	if (err < 0)
		return -errno;
	else if (err == 0)
		return -EINVAL;

	*addr = (struct node_address *)malloc(sizeof(struct node_address));
	if (!*addr)
		return -ENOMEM;

	(*addr)->port = htons(port);
	(*addr)->sock = -1;
	(*addr)->elder_next = NULL;
	(*addr)->elder_prev = NULL;
	pthread_mutex_init(&(*addr)->sock_lock, NULL);

	return 0;
}

int net_free_node_address(struct node_address *addr) {
	free(addr);
	return 0;
}

void net_udp_reply_received(struct udp_packet_header *uph,
				struct sockaddr_in *addr) {
	struct udp_request_entry *current, *previous = NULL, *found = NULL;

	pthread_mutex_lock(&udp_rlist_lock);
	current = udp_rlist;
	while (current) {
		if (current->tag == uph->tag &&
			addr->sin_port == current->addr->sin_port &&
			addr->sin_addr.s_addr == addr->sin_addr.s_addr) {

			found = current;

			if (previous)
				previous->next = current->next;
			else
				udp_rlist = current->next;
			if (current->next)
				current->next->prev = previous;

			break;
		}

		previous = current;
		current = current->next;
	}
	pthread_mutex_unlock(&udp_rlist_lock);

	if (!found)
		return;

	memcpy(found->buffer, uph + 1, min(from_be16(uph->size), found->bsize));
	pthread_cond_broadcast(&found->event);
}

void net_udp_request_received(struct udp_packet_header *uph,
				struct sockaddr_in *addr) {
	struct udp_thread_pool_request *uthp_req;
	(void)addr;

	uthp_req = malloc(sizeof(*uthp_req));
	if (!uthp_req)
		return;

	uthp_req->buffer = (void*)uph;
	uthp_req->addr = malloc(sizeof(struct response_address));
	if (!uthp_req->addr) {
		free(uthp_req);
		return;
	}

	uthp_req->addr->sock = -1;
	memcpy(&uthp_req->addr->addr, addr, sizeof(*addr));
	uthp_req->addr->is_udp = 1;
 	uthp_req->addr->tag = uph->tag;
	uthp_req->next = NULL;

	pthread_mutex_lock(&udp_thpool_lock);
	if (udp_thpool_rlist_end) {
		udp_thpool_rlist_end->next = uthp_req;
		udp_thpool_rlist_end = uthp_req;
	} else {
		udp_thpool_rlist = uthp_req;
		udp_thpool_rlist_end = uthp_req;
		pthread_cond_signal(&udp_thpool_event);
	}
	pthread_mutex_unlock(&udp_thpool_lock);
}

void *net_udp_serve_client(void *args) {
	struct udp_thread_pool_request *uthp_req;
	struct udp_packet_header *uph;
	(void)args;

	while (1) {
		pthread_mutex_lock(&udp_thpool_lock);
		while (!udp_thpool_rlist)
			pthread_cond_wait(&udp_thpool_event, &udp_thpool_lock);

		uthp_req = udp_thpool_rlist;
		udp_thpool_rlist = uthp_req->next;
		if (!udp_thpool_rlist)
			udp_thpool_rlist_end = NULL;
		pthread_mutex_unlock(&udp_thpool_lock);

		uph = (struct udp_packet_header *)uthp_req->buffer;
		process_request(uthp_req->addr,
				(char*)uthp_req->buffer + sizeof(*uph),
				from_be16(uph->size));

		free(uthp_req->addr);
		free(uthp_req);
	}
}

void *net_udp_start_listening(void *args) {
	struct sockaddr_in serv_addr, cli_addr;
	socklen_t clilen = sizeof(cli_addr);
	void *buffer = malloc(NET_MAX_UDP_SIZE);
	struct udp_packet_header *uph = (struct udp_packet_header *)buffer;
	(void)args;

	if (!buffer)
		return NULL;

	udp_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (udp_socket < 0)
		return NULL;

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(NET_PORT_NO);

	while (bind(udp_socket, (struct sockaddr*)&serv_addr,
			sizeof(serv_addr)) < 0)
		serv_addr.sin_port++;

	while (1) {
		recvfrom(udp_socket, buffer, NET_MAX_UDP_SIZE, 0,
				(struct sockaddr*)&cli_addr, &clilen);

		switch (uph->flag) {
			case UDP_REQUEST:
				net_udp_request_received(uph, &cli_addr);
				break;
			case UDP_REPLY:
				net_udp_reply_received(uph, &cli_addr);
				break;
		}
	}

	free(buffer);
}

void net_serve_client(struct response_address *addr) {
	struct packet_header ph;
	void *buffer;
	int err;
	uint32_t ptr, size;

	while (1) {
		ptr = 0;
		err = recv(addr->sock, &ph, sizeof(ph), 0);
		if (err <= 0)
			break;

		size = from_be32(ph.size);
		buffer = malloc(size);
		if (buffer == NULL)
			break;

		do {
			err = recv(addr->sock, (char*)buffer + ptr,
					min(size - ptr, NET_MAX_DATA_SIZE),
					0);
			ptr += err;
		} while (err > 0 && ptr < size);
		if (err <= 0) {
			free(buffer);
			break;
		}

		process_request(addr, buffer, size);

		free(buffer);
	}

	close(addr->sock);
}

void *net_client_listening(void *pargs) {
	struct response_address *addr = (struct response_address *)pargs;
	struct thread_pool_request *thp_req;

	net_serve_client(addr);
	free(pargs);

	while (1) {

		pthread_mutex_lock(&thpool_lock);
		thpool_busy--;
		if (thpool_count - thpool_busy > NET_TCP_THREAD_POOL) {
			thpool_count--;
			pthread_mutex_unlock(&thpool_lock);
			return NULL;
		}

		while (!thpool_rlist)
			pthread_cond_wait(&thpool_event, &thpool_lock);

		thp_req = thpool_rlist;
		addr = thp_req->addr;
		thpool_rlist = thp_req->next;
		pthread_mutex_unlock(&thpool_lock);

		net_serve_client(addr);

		free(thp_req->addr);
		free(thp_req);
	}
	return NULL;
}

void net_got_connection(struct response_address *addr) {
	struct thread_pool_request *thp_req;
	struct response_address *raddr = malloc(sizeof(*addr));
	pthread_t thread;

	if (!raddr)
		return;

	memcpy(raddr, addr, sizeof(*addr));
	pthread_mutex_lock(&thpool_lock);
	if (thpool_count - thpool_busy < 1 &&
		!pthread_create(&thread, NULL, net_client_listening, raddr)) {
		thpool_count++;
		thpool_busy++;
		pthread_mutex_unlock(&thpool_lock);
	} else {
		thp_req = malloc(sizeof(*thp_req));
		if (!thp_req) {
			free(raddr);
			pthread_mutex_unlock(&thpool_lock);
			return;
		}

		thp_req->addr = raddr;
		thp_req->next = thpool_rlist;

		thpool_rlist = thp_req;
		pthread_mutex_unlock(&thpool_lock);

		pthread_cond_signal(&thpool_event);
	}
}

int net_start_listening(net_process_request preq) {
	int sock, cli_sock, err;
	struct response_address resp;
	struct sockaddr_in serv_addr, cli_addr;
	socklen_t clilen = sizeof(cli_addr);
	pthread_t thread;

	process_request = preq;

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0)
		return -errno;

	err = net_set_buffer_size(sock);
	if (err < 0)
		return err;

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(NET_PORT_NO);
	if (bind(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
		return -errno;

	listen(sock, 5);

	pthread_create(&thread, NULL, net_udp_start_listening, NULL);

	while (1) {
		cli_sock = accept(sock, (struct sockaddr *)&cli_addr, &clilen);
		if (cli_sock < 0)
			return -errno;

		resp.sock = cli_sock;
		resp.is_udp = 0;
		net_got_connection(&resp);
	}
}

struct net_guradian_args {
	net_guard_check check;
};

void *net_guardian(void *pargs) {
	net_guard_check check = ((struct net_guradian_args*)pargs)->check;
	free(pargs);

	while (1) {
		pthread_mutex_lock(&guard_lock);
		check(guarded);
		pthread_mutex_unlock(&guard_lock);

		sleep(NET_GUARD_RES);
	}

	return NULL;
}

int net_set_guard(struct node_address *addr, net_guard_check check) {
	int res;
	struct net_guradian_args *args;

	if (guarded == addr)
		return 0;

	args = malloc(sizeof(*args));
	if (!args)
		return -ENOMEM;
	args->check = check;

	res = pthread_mutex_lock(&guard_lock);
	res = pthread_mutex_lock(&elders_lock);

	if (guarded) {
		pthread_cancel(guardian);
		net_elder_list_add(guarded);
	}

	guarded = addr;
	if (addr->sock != -1)
		net_elder_list_rem(addr);
	res = pthread_create(&guardian, NULL, net_guardian, args);

	pthread_mutex_unlock(&elders_lock);
	pthread_mutex_unlock(&guard_lock);

	return -res;
}

static int net_udp_send_request(struct node_address *addr, const void *data,
				uint32_t size, void *reply, uint32_t rsize) {
	struct udp_request_entry uentry;
	struct sockaddr_in serv_addr;
	struct timespec timeout;
	struct udp_packet_header *uph;
	int res;
	void *buffer;

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = addr->ipv4;
	serv_addr.sin_port = addr->port;

	uentry.addr = &serv_addr;
	uentry.tag = (uint8_t)rand();
	uentry.valid = 1;

	pthread_cond_init(&uentry.event, NULL);
	pthread_mutex_init(&uentry.event_lock, NULL);

	uentry.buffer = reply;
	uentry.bsize = rsize;
	uentry.prev = NULL;

	pthread_mutex_lock(&udp_rlist_lock);
	uentry.next = udp_rlist;
	if (uentry.next)
		uentry.next->prev = &uentry;
	udp_rlist = &uentry;
	pthread_mutex_unlock(&udp_rlist_lock);

	buffer = malloc(size + sizeof(struct udp_packet_header));
	if (!buffer)
		return -ENOMEM;

	uph = (struct udp_packet_header*)buffer;
	uph->size = to_be16(size);
	uph->tag = uentry.tag;
	uph->flag = UDP_REQUEST;
	memcpy((char*)buffer + sizeof(*uph), data, size);
	res = sendto(udp_socket, buffer, size + sizeof(*uph), 0,
			(struct sockaddr*)&serv_addr, sizeof(serv_addr));
	if (res < 0)
		return -errno;

	clock_gettime(CLOCK_REALTIME, &timeout);
	timeout.tv_sec += NET_REPLY_TIMEOUT;

	pthread_mutex_lock(&uentry.event_lock);
	res = pthread_cond_timedwait(&uentry.event, &uentry.event_lock,
					&timeout);
	if (res == ETIMEDOUT) {
		pthread_mutex_lock(&udp_rlist_lock);
		if (uentry.prev)
			uentry.prev->next = uentry.next;
		else
			udp_rlist = uentry.next;
		if (uentry.next)
			uentry.next->prev = uentry.prev;
		pthread_mutex_unlock(&udp_rlist_lock);

		res = -ETIMEDOUT;
	}
	pthread_mutex_unlock(&uentry.event_lock);

	pthread_mutex_destroy(&uentry.event_lock);
	pthread_cond_destroy(&uentry.event);

	return res;
}

int net_send_request(struct node_address *addr, const void *data, uint32_t size,
			 void *reply, uint32_t rsize) {
	int res;
	fd_set fds;
	uint32_t ptr;
	struct timeval timeout;
	struct packet_header ph;

	if (size + sizeof(struct udp_packet_header) <= NET_MAX_UDP_SIZE &&
		rsize + sizeof(struct udp_packet_header) <= NET_MAX_UDP_SIZE &&
		!net_connection_exist(addr))
		return net_udp_send_request(addr, data, size, reply, rsize);

	res = net_acquire_connection(addr);
	if (res < 0) {
		net_release_connection(addr);
		return res;
	}

	ph.size = to_be32(size);
	res = send(addr->sock, &ph, sizeof(ph), MSG_NOSIGNAL | MSG_MORE);
	if (res < 0) {
		net_release_connection(addr);
		return -errno;
	}

	ptr = 0;
	do {
		res = send(addr->sock, (char*)data + ptr, min(size - ptr,
			NET_MAX_DATA_SIZE), MSG_NOSIGNAL);
		ptr += res;
	} while (res > 0 && ptr < size);
	if (res < 0) {
		net_release_connection(addr);
		return -errno;
	}

	ptr = 0;
	FD_ZERO(&fds);
	FD_SET(addr->sock, &fds);
	timeout.tv_sec = NET_REPLY_TIMEOUT;
	timeout.tv_usec = 0;
	res = select(addr->sock + 1, &fds, NULL, NULL, &timeout);
	if (res < 0) {
		net_release_connection(addr);
		return -errno;
	} else if (res == 0) {
		net_release_connection(addr);
		return -ETIMEDOUT;
	}

	res = recv(addr->sock, &ph, sizeof(ph), 0);
	if (res < 0) {
		net_release_connection(addr);
		return -errno;
	} else if (res == 0) {
		net_release_connection(addr);
		return -ECONNRESET;
	}

	size = from_be32(ph.size);
	if (size > rsize) {
		net_release_connection(addr);
		return -EMSGSIZE;
	}
	rsize = rsize > size ? size : rsize;

	do {
		res = recv(addr->sock, (char*)reply + ptr,
			min(rsize - ptr, NET_MAX_DATA_SIZE), 0);
		ptr += res;
	} while (res > 0 && ptr < rsize);

	if (res < 0) {
		net_release_connection(addr);
		return -errno;
	} else if (res == 0) {
		net_release_connection(addr);
		return -ECONNRESET;
	}

	return 0;
}

static int net_udp_send_reply(struct response_address *addr, const void *data,
				uint32_t size) {
	void *buffer;
	int res;
	struct udp_packet_header *uph;
	socklen_t len = sizeof(addr->addr);

	if (size + sizeof(struct udp_packet_header) > NET_MAX_UDP_SIZE)
		return -EMSGSIZE;

	buffer = malloc(size + sizeof(struct udp_packet_header));
	if (!buffer)
		return -ENOMEM;

	uph = (struct udp_packet_header*)buffer;
	uph->size = to_be16(size);
	uph->tag = addr->tag;
	uph->flag = UDP_REPLY;
	memcpy((char*)buffer + sizeof(*uph), data, size);

	res = sendto(udp_socket, buffer, size + sizeof(*uph), 0,
		(struct sockaddr*)&addr->addr, len);
	if (res < 0)
		return -errno;

	return 0;
}

int net_send_reply(struct response_address *addr, const void *data,
			uint32_t size) {
	int res;
	uint32_t ptr = 0;
	struct packet_header ph;

	if (addr->is_udp)
		return net_udp_send_reply(addr, data, size);

	ph.size = to_be32(size);
	res = send(addr->sock, &ph, sizeof(ph), MSG_NOSIGNAL | MSG_MORE);
	if (res < 0)
		return -errno;

	do {
		res = send(addr->sock, (char*)data + ptr, min(size - ptr,
			NET_MAX_DATA_SIZE), MSG_NOSIGNAL);
		ptr += res;
	} while (res > 0 && ptr < size);

	if (res < 0)
		return -errno;

	return 0;
}

int net_init(void) {
	pthread_t thread;
	int i, res;

	for (i = 0; i < NET_UDP_THREAD_POOL; i++) {
		res = pthread_create(&thread, NULL, net_udp_serve_client, NULL);
		if (res != 0)
			return -res;
	}

	return -pthread_create(&thread, NULL, net_elders_collector, NULL);
}
