/* Shoggoth - Distributed File System
 * Common utils
 *
 * Copyright (C) 2012 Pawel Dziepak
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <arpa/inet.h>

static inline int min(int a, int b) {
	return a > b ? b : a;
}

static inline int max(int a, int b) {
	return a < b ? b : a;
}

#define to_be16(x) htons(x)
#define to_be32(x) htonl(x)

static inline uint64_t to_be64(uint64_t x) {
	uint64_t y;
	unsigned char *py = (unsigned char*)&y;

	py[0] = x >> 56;
	py[1] = x >> 48;
	py[2] = x >> 40;
	py[3] = x >> 32;
	py[4] = x >> 24;
	py[5] = x >> 16;
	py[6] = x >> 8;
	py[7] = x;

	return y;
}

#define from_be16(x) ntohs(x)
#define from_be32(x) ntohl(x)

static inline uint64_t form_be64(uint64_t x) {
	unsigned char *px = (unsigned char*)&x;

	return (uint64_t)px[0] << 56 | (uint64_t)px[1] << 48 |
		(uint64_t)px[2] << 40 | (uint64_t)px[3] << 32 |
		(uint64_t)px[4] << 24 | (uint64_t)px[5] << 16 |
		(uint64_t)px[6] << 8 | (uint64_t)px[7];
}

#endif
