/* Shoggoth - Distributed File System
 * Network module
 *
 * Copyright (C) 2012 Pawel Dziepak
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef NET_H_
#define NET_H_

#include <netinet/in.h>
#include <pthread.h>
#include <stdint.h>
#include <sys/socket.h>

#define NET_PORT_NO		1423

struct node_address;
struct response_address;

typedef void (*net_process_request)(struct response_address *addr,
					const void *data, uint32_t size);

typedef void (*net_guard_check)(struct node_address *addr);

int net_init(void);

int net_create_node_address(struct node_address **addr, const char *ipv4,
				uint16_t port);
int net_free_node_address(struct node_address *addr);

int net_start_listening(net_process_request preq);

int net_set_guard(struct node_address *addr, net_guard_check check);

int net_send_request(struct node_address *addr, const void *data, uint32_t size,
			void *reply, uint32_t rsize);
int net_send_reply(struct response_address *addr, const void *data,
			uint32_t size);

#endif
